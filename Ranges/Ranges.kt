package Conventions.Ranges

import Conventions.Comparison.MyDate

fun checkInRange(date: MyDate, first: MyDate, last: MyDate): Boolean {
    return date in first..last
}