package Conventions.OperatorsOverloading

import TimeInterval.*

enum class TimeInterval { DAY, WEEK, YEAR }

data class MyDate(val year: Int, val month: Int, val dayOfMonth: Int)

operator fun MyDate.plus(timeInterval: TimeInterval) : MyDate =
    addTimeIntervals(timeInterval, 1)

fun task1(today: MyDate): MyDate {
    return today + TimeInterval.YEAR + TimeInterval.WEEK
}

fun task2(today: MyDate): MyDate {
    return today + TimeInterval.YEAR * 2 + TimeInterval.WEEK * 3 + TimeInterval.DAY * 5
}

class RepeatInterval(val interval: TimeInterval, val number: Int)

operator fun TimeInterval.times(number: Int) =
    RepeatInterval(this, number)

operator fun MyDate.plus(timeIntervals: RepeatInterval) =
    addTimeIntervals(timeIntervals.interval, timeIntervals.number)
